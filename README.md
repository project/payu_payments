CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage


INTRODUCTION
------------

This module provides configurable block that allows visitors to donate
using PayU payment system. It can be used, for example, to raise funds
for charitable causes.


REQUIREMENTS
------------

This module require the OpenPayU PHP library -
https://github.com/PayU-EMEA/openpayu_php.

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the PayU Donations module as you would normally install a 
contributed Drupal module. Visit https://www.drupal.org/node/1897420 
for further information.


CONFIGURATION
-------------

The payment block has several configuration fields,
all of which are required. They are related to:
- PayU payment point data,
- payment information,
- form configuration.


USAGE
-----

    1. Visit the /admin/structure/block.
    2. Assign block to a particular region by clicking on a button Place block.
    3. Fill block configuration fields.
