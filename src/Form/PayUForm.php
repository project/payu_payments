<?php

namespace Drupal\payu_payments\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a PayU Payments form.
 */
class PayUForm extends FormBase {

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $account;

  /**
   * Success status.
   */
  private const SUCCESS_STATUS = 'SUCCESS';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->logger = $container->get('logger.factory')->get('payu_payments');
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payu_payments_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $submit_button_text
   *   Text displayed on the form approval button.
   * @param string $currency_code
   *   ISO 4217 Currency Code.
   * @param string $payment_description
   *   Payment description.
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    string $submit_button_text = '',
    string $currency_code = '',
    string $payment_description = ''
  ) {

    $form['payu_amount'] = [
      '#type' => 'number',
      '#min' => 0.01,
      '#step' => 0.01,
      '#title' => $this->t('Amount'),
      '#required' => TRUE,
      '#default_value' => $form_state->getValues()['payu_amount'] ?? NULL,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $submit_button_text,
    ];

    $form_state->set('currency_code', $currency_code);
    $form_state->set('payment_description', $payment_description);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match('/^\d+\.{0,1}\d*?$/', $form_state->getValue('payu_amount')) && !empty($form_state->getValue('payu_amount'))) {
      $form_state->setErrorByName('payu_amount', $this->t('The amount can only contain numbers!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order = [];
    $order['continueUrl'] = $this->getRequest()->getSchemeAndHttpHost();
    $order['description'] = $form_state->get('payment_description');
    $order['customerIp'] = $this->getRequest()->getClientIp();
    $order['merchantPosId'] = \OpenPayU_Configuration::getOauthClientId() ? \OpenPayU_Configuration::getOauthClientId() : \OpenPayU_Configuration::getMerchantPosId();
    $order['currencyCode'] = $form_state->get('currency_code');
    $order['totalAmount'] = $form_state->getValue('payu_amount');
    if ($order['currencyCode'] !== 'HUF') {
      $order['totalAmount'] *= 100;
    }

    try {
      $response = \OpenPayU_Order::create($order);
      $status_desc = \OpenPayU_Util::statusDesc($response->getStatus());
      if ($response->getStatus() === self::SUCCESS_STATUS) {
        $responseUri = new TrustedRedirectResponse($response->getResponse()->redirectUri);
        $form_state->setResponse($responseUri);
      }
      else {
        $message = 'Status: ' . $response->getStatus() . ': ' . $status_desc . ' | User Id: ' . $this->account->id();
        $this->messenger()->addError($message);
        $this->logger->error($message);
      }
    }
    catch (\OpenPayU_Exception $e) {
      $this->messenger()->addError($e->getMessage());
      $message = 'Error: ' . $e->getMessage() . ' | User Id: ' . $this->account->id();
      $this->logger->error($message);
    }
  }

}
