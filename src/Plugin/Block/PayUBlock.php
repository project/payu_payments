<?php

namespace Drupal\payu_payments\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a PayU block.
 *
 * @Block(
 *   id = "payu_payments_block",
 *   admin_label = @Translation("PayU Block"),
 *   category = @Translation("Payment")
 * )
 */
class PayUBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * ISO 4217 Currency Codes as Strings.
   */
  private const CURRENCY_CODES = [
    "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD",
    "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BMD", "BND",
    "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BZD", "CAD",
    "CDF", "CHE", "CHF", "CHW", "CNY", "COP", "COU", "CRC",
    "CUC", "CUP", "CZK", "DKK", "DOP", "DZD", "EGP", "ERN",
    "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GHS", "GIP",
    "GMD", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF",
    "ILS", "INR", "JMD", "KES", "KGS", "KHR", "KYD", "KZT",
    "LKR", "LRD", "LSL", "LTL", "LVL", "MAD", "MDL", "MKD",
    "MNT", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZN",
    "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "PAB", "PEN",
    "PGK", "PHP", "PKR", "PLN", "QAR", "RON", "RSD", "RUB",
    "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SOS",
    "SRD", "SYP", "SZL", "THB", "TJS", "TMT", "TOP", "TRY",
    "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "USN", "USS",
    "UYU", "UZS", "VEF", "WST", "XCD", "YER", "ZAR", "ZMK",
    "ZWL",
  ];

  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->formBuilder = $container->get('form_builder');
    $instance->logger = $container->get('logger.factory')->get('payu_payments');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $configuration = $this->getConfiguration();

    $form['payu_environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment mode'),
      '#options' => [
        'secure' => $this->t('Production'),
        'sandbox' => $this->t('Sandbox'),
      ],
      '#required' => TRUE,
      '#default_value' => $configuration['payu_environment'] ?? '',
    ];

    $form['payu_pos_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Point of sale ID (pos_id)'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_pos_id'] ?? '',
    ];

    $form['payu_second_key_md5'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second key (MD5)'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_second_key_md5'] ?? '',
    ];

    $form['payu_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth protocol - client_id'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_client_id'] ?? '',
    ];

    $form['payu_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth protocol - client_secret'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_client_secret'] ?? '',
    ];

    $form['payu_currency'] = [
      '#type' => 'select',
      '#options' => array_combine(self::CURRENCY_CODES, self::CURRENCY_CODES),
      '#title' => $this->t('Currency (ISO-4217)'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_currency'] ?? '',
    ];

    $form['payu_payment_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment description'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_payment_description'] ?? '',
    ];

    $form['payu_submit_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text displayed on the form approval button'),
      '#required' => TRUE,
      '#default_value' => $configuration['payu_submit_button_text'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $configs = [
      'payu_environment',
      'payu_pos_id',
      'payu_second_key_md5',
      'payu_client_id',
      'payu_client_secret',
      'payu_currency',
      'payu_payment_description',
      'payu_submit_button_text',
    ];

    foreach ($configs as $config) {
      $this->configuration[$config] = $form_state->getValue($config);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!class_exists(\OpenPayU_Configuration::class)) {
      $message = 'You must install OpenPayU library to use PayU Payments module!';
      $this->logger->error($message);

      return [
        '#markup' => "<span>{$message}</span>",
      ];
    }

    \OpenPayU_Configuration::setEnvironment($this->configuration['payu_environment']);
    \OpenPayU_Configuration::setMerchantPosId($this->configuration['payu_pos_id']);
    \OpenPayU_Configuration::setSignatureKey($this->configuration['payu_second_key_md5']);
    \OpenPayU_Configuration::setOauthClientId($this->configuration['payu_client_id']);
    \OpenPayU_Configuration::setOauthClientSecret($this->configuration['payu_client_secret']);

    $form = $this->formBuilder->getForm(
      'Drupal\payu_payments\Form\PayUForm',
      $this->configuration['payu_submit_button_text'],
      $this->configuration['payu_currency'],
      $this->configuration['payu_payment_description'],
    );

    return [
      '#theme' => 'payu_payments_block',
      '#form' => $form,
    ];
  }

}
